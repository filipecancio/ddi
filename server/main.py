from flask import Flask
from flask import jsonify

GAMES = [
    {
        "id": 1,
        "date": "15/10/2019",
        "title": "Stadia, serviço de games na nuvem do Google, será lançado em 19 de Novembro",
        "url_name": "https://g1.globo.com/pop-arte/games/noticia/2019/10/15/stadia-servico-de-games-na-nuvem-do-google-sera-lancado-em-19-de-novembro.ghtml",
    },
    {
        "id": 2,
        "date": "26/04/2019",
        "title": "Mortal Kombat: Como fazer todos os fatalities?",
        "url_name": "https://www.uol.com.br/start/ultimas-noticias/2019/04/26/mortal-kombat-11-como-fazer-todas-as-fatalities.htm",
    },
    {
        "id": 3,
        "date": "21/10/2016",
        "title": "Conheça 5 distribuições GNU/Linux voltadas para jogos",
        "url_name": "https://sempreupdate.com.br/conheca-5-distribuicoes-gnu-linux-voltadas-para-jogos/",
    }
]

SYSTEMS = [
    {
        "id": 1,
        "date": "22/05/2019",
        "title": "Estes são os 12 problemas já encontrados na atualização do Windows 10",
        "url_name": "https://olhardigital.com.br/noticia/microsoft-lista-todos-os-problemas-da-nova-atualizacao-do-windows-10/86052",
    },
    {
        "id": 2,
        "date": "10/05/2015",
        "title": "Atualização do Windows 10 está causando problemas para alguns usuários",
        "url_name": "https://canaltech.com.br/windows/atualizacao-do-windows-10-esta-causando-problemas-para-alguns-usuarios-46921/",
    },
    {
        "id": 3,
        "date": "04/05/2016",
        "title": "Top 5 distribuições Linux que podem substituir o Windows 10",
        "url_name": "https://pplware.sapo.pt/linux/top-5-distribuies-gnulinux-que-podem-substituir-o-windows-10/",
    }
]

IS_ALIVE = "yes"
VERSION = "0.0.0"
AUTHOR = "Cancio"
EMAIL = "filipe.cancio@gmail.com"

service = Flask(__name__)


@service.route(f"/{VERSION}/info", methods=['GET'])
def get_info():
    info = jsonify(
        version=VERSION,
        author=AUTHOR,
        email=EMAIL
    )
    return info


@service.route(f"/{VERSION}/is_alive", methods=['GET'])
def is_alive():
    return IS_ALIVE


@service.route(f"/{VERSION}/games", methods=['GET'])
def get_games():
    return jsonify(GAMES)


@service.route(f"/{VERSION}/systems", methods=['GET'])
def get_systems():
    return jsonify(SYSTEMS)


if __name__ == "__main__":
    service.run(
        host="0.0.0.0",
        debug=False
    )
