import urllib.request, json, time

HOST = "127.0.0.1"
PORT = "5000"
VERSION = "0.0.0"
URL_SERVICE = f"http://{HOST}:{PORT}/{VERSION}"

IS_ALIVE = f"{URL_SERVICE}/is_alive"
GAMES = f"{URL_SERVICE}/games"
SYSTEMS = f"{URL_SERVICE}/systems"


def get_url(url):
    return urllib.request.urlopen(url).read().decode("utf-8")


def is_alive():
    alive = False
    if get_url(IS_ALIVE) == "yes":
        alive = True
    return alive


def get_games():
    news = get_url(GAMES)
    return json.loads(news)


def get_systems():
    systems = get_url(SYSTEMS)
    return json.loads(systems)


def print_news(news):
    for new in news:
        print(new["title"])


if __name__ == "__main__":
    while True:
        if is_alive():
            print("************ Jogatina ************")
            print_news(get_games())
            print("************ Sistemas ************")
            print_news(get_systems())
        else:
            print("Service not found :(")
        time.sleep(3)
